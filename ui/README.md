## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run serve

# build for production and launch server
$ npm run build

```

For live demo, check out [Haitwik](https://chainlink.haitwik.com).
