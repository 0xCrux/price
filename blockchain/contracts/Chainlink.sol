pragma solidity ^0.6.7;

import 'SupportLinkContract.sol';

contract ChainLinkPriceOracle {

    function getUpdatedPrice(address contractAddress) public view returns (int, string memory, uint, uint, uint) {
        (uint80 roundID, int price, uint startedAt, uint timeStamp, uint80 answeredInRound) = AggregatorV3Interface(contractAddress).latestRoundData();
        string memory pair = AggregatorV3Interface(contractAddress).description();
        uint256 lastUpdated = EACAggregatorProxy(contractAddress).latestTimestamp();
        uint version = EACAggregatorProxy(contractAddress).version();
        uint decimal = EACAggregatorProxy(contractAddress).decimals();
        return (price, pair, timeStamp, version, decimal);
    }
}